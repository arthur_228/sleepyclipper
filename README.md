# sleepyclipper

## What is it and how does it work?
It's a little script to help you create clips after stream and don't get distracted during it as well as getting timestamps for compilations. With a small setup at the start you can leave the thing running in the background. After that with a press of a button, it'll create a link to the VOD for you to visit and clip if you decide later
Initially I wrote it as a tiny project for myself, but decided to make user-friendly as well as expand it with categories for other people to use

## Installation
This script is a Python code, so first you need Python on your PC, thus:

>1.1 Download it from official site
 [https://www.python.org/downloads/](https://www.python.org/downloads/)

>1.2 Launch installer: you can pick "Install Now" if you don't want to think extra time

With Python installed we halfway there, now we need some extra files:

>2.1 Open terminal (can search for it in start menu, or use shortcut `WIN+R`, type `cmd.exe` in the field and hit Enter)

>2.2 Paste this in the terminal you just opened: `python.exe -m pip install keyboard`

You just installed library that allows this script to work with your keyboard!

Now download actual script from here, for that
> 3 Click on `sleepyclipper.py` right above and hit lil download button (on top left). You can check code itself here as well if you're curious 


## Using the script
To launch the script double-click `sleepyclipper.py` wherever you downloaded it to (open with Python if it asks for some reason.)

Follow the prompts in opened terminal until you see: `"✓ everything is set up!"`

After that you can leave it running in background.

Whenever you press key related to category, it'll ask if you want to mark it with name (if required) and then will store link to this part of vod to category related file.

Finally, when stream is over you can visit vod timestamps from the file.

\* Categories, hotkeys, filenames, etc can be changed in `settings.ini` (standard ini structure + comments can give you guidance)
