import os
import sys
import time
import keyboard
import configparser

twitch_delay=60
last_time = 0
not_assigned = True

#RETURNS CURRENT TIME
def get_time():
    return int(time.perf_counter())

#RECEIVES CURRENT TIME
#PRINTS TIMECODE IN SELECTED FORMAT
def get_timestamp():
    cur_time = int(get_time() - timer_start)
    mins = cur_time // 60
    secs = cur_time % 60
    hrs = mins // 60
    mins = mins % 60
    return f'{hrs}h{mins}m{secs}s'
    

#CHECKS WHICH TYPE CLIP IS, WRITES IT TO FILE
def process_clip(key_name):
    for section in cfg.sections():
        if cfg[section]['keybind'] == key_name:
            os.system('cls')
            sys.stdin.flush()
            timestamp = get_timestamp()
            f = open(cfg[section]['output_file'], "a")
            if cfg[section]['used'] == 'no':
                f.write(time.strftime('%d.%m.%Y (%A) ==================================================================\n'))
                cfg[section]['used'] = 'yes'
            f.write(f'{link}?t={timestamp} ')
            if cfg[section]['ask_name'] == 'yes':
                f.write(input('Clip name '))
                os.system('cls')
            f.write('\n')
            f.close()
            print(f'New Clip @{timestamp}')

#KEY PROCESSOR (REGISTERING ON RELEASE TO PROC ONCE)
def on_release(key):
    if key.name in keylist:
        process_clip(key.name)

#MAIN BODY

#initialization from file
cfg = configparser.ConfigParser()
cfg.read('settings.ini')

#marking each category to determine which was used this day & saving keys set for the session
keylist = []
print('Your Keybinds are:')
for section in cfg.sections():
    if cfg[section]['keybind'] != 'N/A':
        cfg[section]['used'] = 'no'
        keylist.append(cfg[section]['keybind'])
        print(f'{cfg[section]} - {cfg[section]["keybind"]}')

#manual per stream initialization
while True:
    starter_time = input("\n? Input current livetime (hh:mm:ss): ")
    try:
        shift = int(starter_time[0:2])*3600+int(starter_time[3:5])*60+int(starter_time[6:8])
        print("✓ Time correction applied (live time differs from VOD time a bit)\n")
    except ValueError:
        print("X PAY MIND TO FORMAT, should look like 00:12:34\n")
        continue
    break
timer_start = get_time()-shift+int(cfg['global_settings']['twitch_delay'])

link = input("? Provide vod link here (from Recent Broadcasts): ")
os.system('cls')
print("✓ everything is set up!")


#running listener, waiting for keys
keyboard.on_release(on_release)
keyboard.wait()
quit()